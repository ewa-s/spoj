// SPOJ submission 22449335 (JAVA) plaintext list. Status: AC, problem FANGEN, contest SPOJPL. By ewa_smaga (ewa_smaga), 2018-10-05 20:43:48.
// https://pl.spoj.com/problems/FANGEN/
import java.util.*;
 
enum FanPrinterType {
	QuadrupleSymmetry
}
 
enum FanDirection {
	Clockwise, Counterclockwise
}
 
enum WingType {
	AsteriskTriangle
}
 
 
class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{		
		WingedFanPrinter fanPrinter = WingedFanPrinterFactory.getWingedFanPrinter(FanPrinterType.QuadrupleSymmetry);
	
		Scanner scanner = new Scanner(System.in);
		int fanRank = scanner.nextInt();
		
		while (fanRank != 0 ) {
			if (fanRank > 0) {
				fanPrinter.printFan(fanRank, FanDirection.Counterclockwise);
			}
			else {
				fanPrinter.printFan(-fanRank, FanDirection.Clockwise);
			}
			fanRank = scanner.nextInt();
		}
		
		scanner.close();
	}
}
 
class WingedFanPrinterFactory {
	public static WingedFanPrinter getWingedFanPrinter(FanPrinterType type) {
		WingPainter painter = WingPainterFactory.getWingPainter(WingType.AsteriskTriangle);
		CharMap wingMap = painter.getWingMap();
		if (type == FanPrinterType.QuadrupleSymmetry) {
			return new WingedFanPrinterQuadrupleSymmetry(wingMap);
		}
		throw new IllegalArgumentException("Illegal FanPrinterType: " + type);
	}
}
 
interface WingedFanPrinter {
	void printFan(int size, FanDirection direction);
}
 
 
class WingedFanPrinterQuadrupleSymmetry implements WingedFanPrinter {
	private CharMap wingMap;
	
	public WingedFanPrinterQuadrupleSymmetry(CharMap wingMap) {
		this.wingMap = wingMap;
	}
	
	public void printFan(int size, FanDirection direction) {
		if (direction == FanDirection.Clockwise) {
			for (int i = 0; i < size; i++) {
				System.out.print(repeatString(wingMap.getWingEmpty(), i));
				System.out.print(repeatString(wingMap.getWingFill(), size - i));
				System.out.print(repeatString(wingMap.getWingEmpty(), size - i - 1));
				System.out.print(repeatString(wingMap.getWingFill(), i + 1));
				System.out.print('\n');
			}
			for (int i = 0; i < size; i++) {
				System.out.print(repeatString(wingMap.getWingFill(), size - i));
				System.out.print(repeatString(wingMap.getWingEmpty(), i));
				System.out.print(repeatString(wingMap.getWingFill(), i + 1));
				System.out.print(repeatString(wingMap.getWingEmpty(), size - i - 1));
				System.out.print('\n');
			}
			System.out.print('\n');
		}
		else {
			for (int i = 0; i < size; i++) {
				System.out.print(repeatString(wingMap.getWingFill(), i + 1));
				System.out.print(repeatString(wingMap.getWingEmpty(), size - i - 1));
				System.out.print(repeatString(wingMap.getWingFill(), size - i));
				System.out.print(repeatString(wingMap.getWingEmpty(), i));
				System.out.print('\n');
				
			}
			for (int i = 0; i < size; i++) {
				System.out.print(repeatString(wingMap.getWingEmpty(), size - i - 1));
				System.out.print(repeatString(wingMap.getWingFill(), i + 1));
				System.out.print(repeatString(wingMap.getWingEmpty(), i));
				System.out.print(repeatString(wingMap.getWingFill(), size - i));
				System.out.print('\n');
			}
			System.out.print('\n');
		}
	}
	
	private static String repeatString(char ch, int n) {
		String result = "";
		
		for (int i = 0; i < n; i++) {
			result += Character.toString(ch); 
		}
		
		return result;
	}
}
 
interface WingPainter {
	public CharMap getWingMap();
}
 
class AsteriskWingPainter implements WingPainter {
	public CharMap getWingMap() {
		return new CharMap('*','.');
	}
}
 
class WingPainterFactory {
	public static WingPainter getWingPainter(WingType type) {
		if (type == WingType.AsteriskTriangle) {
			return new AsteriskWingPainter();
		}
		throw new IllegalArgumentException("Illegal WingType: " + type);
	}
}
 
class CharMap {
	char wingFill;
	char wingEmpty;
	
	public CharMap(char fill, char empty) {
		wingFill = fill;
		wingEmpty = empty;
	}
	
	public char getWingFill() {
		return wingFill;
	}
	
	public char getWingEmpty() {
		return wingEmpty;
	}
} 