// SPOJ submission 22465876 (CPP) plaintext list. Status: AC, problem DAPPLES, contest SPOJPL. By ewa_smaga (ewa_smaga), 2018-10-08 18:00:46.
// https://pl.spoj.com/problems/DAPPLES/
#include <iostream>
 
using namespace std;
 
int main()
{
	int numberOfSets;
	cin >> numberOfSets;
 
	for (int i = 0; i < numberOfSets; i++) {
		int deadlySpeed;
		int numberOfPeople;
		cin >> deadlySpeed;
		cin >> numberOfPeople;
 
		int minHeight[21];
 
		for (int j = 0; j < 21; j++) {
			minHeight[j] = 1000;
		}
	
		for (int j = 0; j < numberOfPeople; j++) {
			int height;
			int age;
			int growth;
			cin >> height;
			cin >> age;
			cin >> growth;
 
			for (int k = 0; k < 21; k++) {
				if (height < minHeight[k]) {
					minHeight[k] = height;
				}
				if (age < 20) {
					height += growth;
				}
				age++;
			}
		}
 
		int fallHeight = 5 * deadlySpeed * deadlySpeed;
		for (int j = 0; j < 21; j++) {
			cout << j << ": " << (minHeight[j] + fallHeight) << endl;
		}
	}
	return 0;
} 