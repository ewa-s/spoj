// SPOJ submission 22447244 (JAVA) plaintext list. Status: AC, problem SKARBFI, contest SPOJPL. By ewa_smaga (ewa_smaga), 2018-10-05 13:29:17.
// https://pl.spoj.com/problems/SKARBFI/

import java.util.*;
import java.lang.*;
 
class Main
{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numberOfSets = scanner.nextInt();
		
		for (int i = 0; i < numberOfSets; i++) {
			int numberOfClues = scanner.nextInt();
			int stepsNorthSouth = 0;
			int stepsWestEast = 0;
			
			for (int j = 0; j < numberOfClues; j++) {
				int direction = scanner.nextInt();
				int steps = scanner.nextInt();
				
				switch (direction) {
					case 0 : stepsNorthSouth += steps;
								break;
					case 1 : stepsNorthSouth -= steps;
								break;
					case 2 : stepsWestEast += steps;
								break;
					case 3 : stepsWestEast -= steps;
								break;
				}
			}
			
 
			if (stepsNorthSouth == 0 && stepsWestEast == 0) {
				System.out.println("studnia");
			}
			else {
				if (stepsNorthSouth > 0) {
					System.out.println("0 " + stepsNorthSouth);
				}
				else if (stepsNorthSouth < 0) {
					System.out.println("1 " + (-stepsNorthSouth));
				}
				
				if (stepsWestEast > 0) {
					System.out.println("2 " + stepsWestEast);
				}
				else if (stepsWestEast < 0) {
					System.out.println("3 " + (-stepsWestEast));
				}
			}
		}
		
 
	}
} 