// SPOJ submission 22446829 (JAVA) plaintext list. Status: AC, problem PA05_POT, contest SPOJPL. By ewa_smaga (ewa_smaga), 2018-10-05 11:39:11.
// https://pl.spoj.com/problems/PA05_POT/

import java.util.*;
 
public class Main {
	
	private static int lastDigitOfPower(String a, String b) {
		switch (a.substring(a.length()-1)) {
			case "0" : return 0;
			case "1" : return 1;
			case "2" : {
				// 2 -> 4 -> 8 -> 6
				switch (modulo(b, 4)) {
					case 0 : return 6;
					case 1 : return 2;
					case 2 : return 4;
					case 3 : return 8;
				}
			}
			case "3" : {
				// 3 -> 9 -> 7 -> 1
				switch (modulo(b, 4)) {
					case 0 : return 1;
					case 1 : return 3;
					case 2 : return 9;
					case 3 : return 7;
				}
			}
			case "4" : {
				// 4 -> 6
				switch (modulo(b, 2)) {
					case 0 : return 6;
					case 1 : return 4;
				}	
			}
			case "5" : return 5;
			case "6" : return 6;
			case "7" : {
				// 7 -> 9 -> 3 -> 1
				switch (modulo(b, 4)) {
					case 0 : return 1;
					case 1 : return 7;
					case 2 : return 9;
					case 3 : return 3;
				}
			}
			case "8" : {
				// 8 -> 4 -> 2 -> 6
				switch (modulo(b, 4)) {
					case 0 : return 6;
					case 1 : return 8;
					case 2 : return 4;
					case 3 : return 2;
				}	
			}
			case "9" : {
				// 9 -> 1
				switch (modulo(b, 2)) {
					case 0 : return 1;
					case 1 : return 9;
				}	
			}
		}
		return 11;
	}
	
	private static int modulo(String number, int i) {
		int lastTwoDigits;
 
		if (number.length() >= 2) {
			lastTwoDigits = Integer.valueOf(number.substring(number.length()-2));
		} 
		else {
			lastTwoDigits = Integer.valueOf(number);
		}
		return (lastTwoDigits % i);
	}
	
 
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int entries = scanner.nextInt();
		
		scanner.nextLine();
		for (int i = 0; i < entries; i++) {
			String[] input = new String[2];
			input = scanner.nextLine().split(" ");
			System.out.println(lastDigitOfPower(input[0], input[1]));
		}
		scanner.close();
	}
 
}
 