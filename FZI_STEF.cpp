// SPOJ submission 22456945 (CPP) plaintext list. Status: AC, problem FZI_STEF, contest SPOJPL. By ewa_smaga (ewa_smaga), 2018-10-07 08:05:19.
// https://pl.spoj.com/problems/FZI_STEF/
#include <iostream>
using namespace std;
 
int main() {
	int numberOfCities;
	cin >> numberOfCities;
	long long currentSum = 0;
	long long maxProfit = 0;
	int profit;
 
	for (int i = 0; i < numberOfCities; i++) {
		cin >> profit;
		currentSum += profit;
 
		if (currentSum < 0) {
			currentSum = 0;
		}
 
		if (currentSum > maxProfit) {
			maxProfit = currentSum;
		}
	}
 
	cout << maxProfit << endl;
 
	return 0;
} 